module Full.Model

let fname (ty: string) (n: int) (ty2: string) =
    sprintf @"problemsF\F%s%03d_%s.mdl" ty n ty2

let load (f: string) =
    use fs = System.IO.File.OpenRead(f)
    let r = fs.ReadByte()
    let a = Array3D.create r r r 0
    let mutable c = 0
    while fs.Position < fs.Length do
        let b = fs.ReadByte()
        for i in 0..7 do
            let bit = (b >>> i) &&& 1
            let x = c / (r * r)
            let y = (c / r) % r
            let z = c % r
            if c < r * r * r then
                Array3D.set a x y z bit
            c <- c + 1
    a

let viewXY (a: int[,,]) =
    let r = Array3D.length1 a
    for z in 0..r-1 do
        stdout.WriteLine("--------------------")
        stdout.WriteLine("Z={0}", z)
        let _ = stdin.ReadLine()
        for y in r-1 .. -1 .. 0 do
            for x in r-1 .. -1 .. 0 do
                let bit = Array3D.get a x y z
                stdout.Write(bit)
            stdout.WriteLine()
