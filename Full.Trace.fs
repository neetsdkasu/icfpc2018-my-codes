module Full.Trace

let fname (ty:string) (n: int) =
    sprintf @"dfltTracesF\F%s%03d.nbt" ty n

let dfname (ty:string) (n: int) =
    sprintf @"answersF\F%s%03d.nbt" ty n


let isHalt c = c = 0b11111111
let isWait c = c = 0b11111110
let isFlip c = c = 0b11111101
let isSMove c = (c &&& 0b11001111) = 0b00000100
let isLMove c = (c &&& 0b11001111) = 0b00001100
let isFusionP c = (c &&& 0b111) = 0b111
let isFusionS c = (c &&& 0b111) = 0b110
let isFission c = (c &&& 0b111) = 0b101
let isFill c = (c &&& 0b111) = 0b011
let isVoid c = (c &&& 0b111) = 0b010
let isGFill c = (c &&& 0b111) = 0b001
let isGVoid c = (c &&& 0b111) = 0b000

let toShortDiff a i =
    let d = i - 5
    match a with
    | 0b01 -> (d, 0, 0)
    | 0b10 -> (0, d, 0)
    | 0b11 -> (0, 0, d)
    | _ -> (0,0,0)

let toLongDiff a i =
    let d = i - 15
    match a with
    | 0b01 -> (d, 0, 0)
    | 0b10 -> (0, d, 0)
    | 0b11 -> (0, 0, d)
    | _ -> (0,0,0)

let toNearDiff c =
    let dz = (c % 3) - 1
    let dy = ((c / 3) % 3) - 1
    let dx = (c / 9) - 1
    (dx, dy, dz)
    
let readSMove c1 c2 =
    let a = (c1 >>> 4) &&& 0b11
    let i = c2 &&& 0b11111
    toLongDiff a i

let readLMove c1 c2 =
    let a1 = (c1 >>> 4) &&& 0b11
    let a2 = (c1 >>> 6) &&& 0b11
    let i1 = c2 &&& 0b1111
    let i2 = c2 >>> 4
    (toShortDiff a1 i1, toShortDiff a2 i2)
    
let view (f: string) =
    use fs = System.IO.File.OpenRead(f)
    while fs.Position < fs.Length do
        let b = fs.ReadByte()
        stdout.Write("{0:D8}: ", fs.Position)
        if isHalt b then
            stdout.WriteLine("halt")
        elif isHalt b then
            stdout.WriteLine("wait")
        elif isFlip b then
            stdout.WriteLine("flip")
        elif isSMove b then
            let b2 = fs.ReadByte()
            let (x, y, z) = readSMove b b2
            stdout.WriteLine("SMove ({0}, {1}, {2})", x, y, z)
        elif isLMove b then
            let b2 = fs.ReadByte()
            let ((x1, y1, z1), (x2, y2, z2)) = readLMove b b2
            stdout.WriteLine("LMove ({0}, {1}, {2}) ({3}, {4}, {5})", x1, y1, z1, x2, y2, z2)
        elif isFusionP b then
            let (x, y, z) = toNearDiff (b >>> 3)
            stdout.WriteLine("FusionP ({0}, {1}, {2})", x, y, z)
        elif isFusionS b then
            let (x, y, z) = toNearDiff (b >>> 3)
            stdout.WriteLine("FusionS ({0}, {1}, {2})", x, y, z)
        elif isFission b then
            let (x, y, z) = toNearDiff (b >>> 3)
            let m = fs.ReadByte()
            stdout.WriteLine("Fission ({0}, {1}, {2}) m={3}", x, y, z, m)
        elif isFill b then
            let (x, y, z) = toNearDiff (b >>> 3)
            stdout.WriteLine("Fill ({0}, {1}, {2})", x, y, z)
        elif isVoid b then
            let (x, y, z) = toNearDiff (b >>> 3)
            stdout.WriteLine("Void ({0}, {1}, {2})", x, y, z)
        elif isGFill b then
            let (x, y, z) = toNearDiff (b >>> 3)
            let dx = fs.ReadByte()
            let dy = fs.ReadByte()
            let dz = fs.ReadByte()
            stdout.WriteLine("GFill ({0}, {1}, {2}) ({3}, {4}, {5})", x, y, z, dx, dy, dz)
        elif isGVoid b then
            let (x, y, z) = toNearDiff (b >>> 3)
            let dx = fs.ReadByte()
            let dy = fs.ReadByte()
            let dz = fs.ReadByte()
            stdout.WriteLine("GVoid ({0}, {1}, {2}) ({3}, {4}, {5})", x, y, z, dx, dy, dz)
        else
            stdout.WriteLine("what?")
        if fs.Position % 30L = 0L then
            let cmd = stdin.ReadLine()
            if cmd = "quit" then
                fs.Position <- fs.Length
    stdout.WriteLine("end of: {0:D8}", fs.Position)
    
let trace (f: string) =
    use fs = System.IO.File.OpenRead(f)
    let mutable x = 0
    let mutable y = 0
    let mutable z = 0
    let mutable h = 0
    while fs.Position < fs.Length do
        let b = fs.ReadByte()
        if isHalt b then
            ()
        elif isHalt b then
            ()
        elif isFlip b then
            h <- 1 - h
        elif isSMove b then
            let b2 = fs.ReadByte()
            let (dx, dy, dz) = readSMove b b2
            x <- x + dx
            y <- y + dy
            z <- z + dz
        elif isLMove b then
            let b2 = fs.ReadByte()
            let ((dx1, dy1, dz1), (dx2, dy2, dz2)) = readLMove b b2
            x <- x + dx1 + dx2
            y <- y + dy1 + dy2
            z <- z + dz1 + dz2
        elif isFusionP b then
            ()
        elif isFusionS b then
            ()
        elif isFission b then
            ignore <| fs.ReadByte()
        elif isFill b then
            ()
        elif isVoid b then
            ()
        elif isGFill b then
            ignore <| fs.ReadByte()
            ignore <| fs.ReadByte()
            ignore <| fs.ReadByte()
        elif isGVoid b then
            ignore <| fs.ReadByte()
            ignore <| fs.ReadByte()
            ignore <| fs.ReadByte()
        else
            stdout.WriteLine("what?")
    stdout.WriteLine("end of: ({0}, {1}, {2}) h={3}", x, y, z, h)

let writeSMove (bs: System.IO.MemoryStream) x y z =
    match (x, y, z) with
    | (_, 0, 0) ->
        bs.WriteByte(0b00010100uy)
        bs.WriteByte(byte <| x + 15)
    | (0, _, 0) ->
        bs.WriteByte(0b00100100uy)
        bs.WriteByte(byte <| y + 15)
    | (0, 0, _) ->
        bs.WriteByte(0b00110100uy)
        bs.WriteByte(byte <| z + 15)
    | _ -> ()

let isWrongSMove x y z =
    let i =
        match (x, y, z) with
        | (_, 0, 0) -> x + 15
        | (0, _, 0) -> y + 15
        | (0, 0, _) -> z + 15
        | _ -> -1
    i < 0 || 30 < i
    
let compress (ty:string) (n: int) =
    let f = fname ty n
    use fs = System.IO.File.OpenRead(f)
    use bs = new System.IO.MemoryStream()
    let mutable x = 0
    let mutable y = 0
    let mutable z = 0
    while fs.Position < fs.Length do
        let b = fs.ReadByte()
        if isHalt b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isHalt b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isFlip b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isSMove b then
            let b2 = fs.ReadByte()
            let (dx, dy, dz) = readSMove b b2
            let x2 = x + dx
            let y2 = y + dy
            let z2 = z + dz
            if isWrongSMove x2 y2 z2 then
                writeSMove bs x y z
                x <- dx
                y <- dy
                z <- dz
            else
                x <- x2
                y <- y2
                z <- z2
        elif isLMove b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
            let b2 = fs.ReadByte()
            bs.WriteByte(byte b2)
        elif isFusionP b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isFusionS b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isFission b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
            let b2 = fs.ReadByte()
            bs.WriteByte(byte b2)
        elif isFill b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isVoid b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
        elif isGFill b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
            bs.WriteByte(byte <| fs.ReadByte())
            bs.WriteByte(byte <| fs.ReadByte())
            bs.WriteByte(byte <| fs.ReadByte())
        elif isGVoid b then
            if x <> 0 || y <> 0 || z <> 0 then
                writeSMove bs x y z
                x <- 0
                y <- 0
                z <- 0
            bs.WriteByte(byte b)
            bs.WriteByte(byte <| fs.ReadByte())
            bs.WriteByte(byte <| fs.ReadByte())
            bs.WriteByte(byte <| fs.ReadByte())
        else
            stdout.WriteLine("what?")
    let df = dfname ty n
    System.IO.File.WriteAllBytes(df, bs.ToArray())
    stdout.WriteLine("ok: F{0}{1:D3}", ty, n)
    
    
