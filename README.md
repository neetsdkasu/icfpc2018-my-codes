ICFPContest 2018 My Codes  
-------------------------

Author  : Leonardone @ NEETSDKASU  
License : MIT  
Language: F# 4.0  


##Development Environment  

###Computer  
OS  : Windows 7 Starter SP1 (32-bit)  
CPU : Intel Atom N570 1.66 GHz  
MEM : 1.0 GB  

###Tools  
F#  : Microsoft (R) F# Compiler Version 14.0.23413.0  



## Lightning  

 - extract dfltTracesL.zip into dicrectory dfltTracesL  
 - do command: mkdir answersL  
 - do command: git checkout lightning-final-submit  
 - do command: c.bat  
 - do command: a.exe  
 - LANNN.nbt files in dicrectory answersL  



## Full  

 - extract dfltTracesF.zip into dicrectory dfltTracesF  
 - do command: mkdir answersF  
 - do command: git checkout full-final-submit  
 - do command: c.bat  
 - do command: a.exe A  
 - FANNN.nbt files in dicrectory answersF  
 - do command: a.exe D  
 - FDNNN.nbt files in dicrectory answersF  
 - do command: a.exe R  
 - FRNNN.nbt files in dicrectory answersF  



## Approach  

combine SMoves in dfltTraces nbt  
(including bug in Lightning Solution)  




## 感想 (this japanese comments using text-encoding UTF-8)  

やったことはdfltTracesのデフォルトのベーシックソリューションのSMoveを繋げてStepを減らしただけ  
LightningではSMoveの移動距離について少し勘違いしてたので(mlen=16の移動が発生)ダメ  

もう少し時間があれば、SMoveを繋げてLMoveを生成してStep減らしたり、接地判定してFlipでエネルギー削減したりしたかった…  
たまたまカジテツやおつかい等が普段より多くて思いのほか時間足りなかったのだ…  

2週間コンテストだったらデフォルトのソリューションをいじるのではなく、自分でボットの動きを生成したかったなあ…と  

ICFPContestは2016と2017と参加して、2018が一番取り組みやすかったので、時間足りなかったの本当にもったいなかった…  



