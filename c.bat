fsc ^
    --nologo ^
    --target:exe ^
    --optimize+ ^
    --warnaserror+ ^
    --out:a.exe ^
    Lightning.Model.fs ^
    Lightning.Trace.fs ^
    Full.Model.fs ^
    Full.Trace.fs ^
    Main.fs