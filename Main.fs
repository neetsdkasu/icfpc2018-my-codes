module Main

open Full.Trace

// FA 186
// FD 186
// FR 115

[<EntryPoint>]
let main args =
    if Array.length args = 0 then
        stdout.WriteLine("need arg")
    else
        let a = Array.get args 0
        match a with
        | "A" ->
            for i in 1..186 do
                compress "A" i
        | "D" ->
            for i in 1..186 do
                compress "D" i
        | "R" ->
            for i in 1..115 do
                compress "R" i
        | _ ->
            stdout.WriteLine("invalid arg: {0}", a)
    0